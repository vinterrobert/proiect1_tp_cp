package polinom_pro;

import org.junit.Assert;

import org.junit.Test;

//Clasa de test JUnitTest
public class JUnitTest {
	//La fiecare test vom converti Polinom rezultat in urma operiatie la String pentru o mai buna functionare a metodei assertEquals
	private final Operatii o = new Operatii();
	//Testul pentru adunare
	@Test
	public void testAdunare() throws Exception {
		Polinom p1 = new Polinom("x^3+2x^2-10");
		Polinom p2 = new Polinom("x^10-27");
		Polinom p4 = new Polinom ("x^10+x^3+2.0x^2-37.0");
		String result = "x^10+x^3+2.0x^2-37.0";
		String p3 = o.convert(o.adunare(p1, p2));
		Assert.assertEquals(p3,result);
	}
	//Testul pentru scadere
	@Test
	public void testScadere() throws Exception {
		Polinom p1 = new Polinom("x^3+2x^2-10");
		Polinom p2 = new Polinom("x^10-27");
		Polinom p4 = new Polinom ("x^10+x^3+2.0x^2-37.0");
		String result = "-x^10+x^3+2.0x^2+17.0";
		String p3 = o.convert(o.scadere(p1, p2));
		Assert.assertEquals(p3,result);
	}
	//Testul pentru inmultire
	@Test
	public void testInmultire() throws Exception {
		Polinom p1 = new Polinom("x^2+1+7x");
		Polinom p2 = new Polinom("2x");
		String result = "2.0x^3+14.0x^2+2.0x";
		String p3 = o.convert(o.inmultire(p1, p2));
		Assert.assertEquals(p3,result);
	}
	//Testul pentru impartire
	@Test
	public void testImpartire() throws Exception {
		Polinom p1 = new Polinom("x^2+1+7x");
		Polinom p2 = new Polinom("x^3");
		String result = "0.0";
		String rest = "x^2+7.0x+1";
		Polinom p3 = new Polinom();
		String p4 = o.convert(o.impartire(p1, p2,p3));
		String p_rest = o.convert(p3);
		Assert.assertEquals(p4,result);
		Assert.assertEquals(p_rest, rest);
	}
	//Testul pentru derivare
	@Test
	public void testDerivare() throws Exception {
		Polinom p1 = new Polinom("x^2+1+7x");
		String result = "2.0x+7.0";
		String p3 = o.convert(o.derivare(p1));
		Assert.assertEquals(p3,result);
	}
	//Testul pentru integrare
	@Test
	public void testIntegrare() throws Exception {
		Polinom p1 = new Polinom("x^2+1+7x");
		String result = "0.33x^3+3.5x^2+x";
		String p3 = o.convert(o.integrare(p1));
		Assert.assertEquals(p3,result);
	}
}
