package polinom_pro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

	private Operatii m_model;
	private View m_view;
	//Constructor cu parametrii
	public Controller(Operatii model, View view_GUI)
	{
		m_model = model;
		m_view = view_GUI;
		
		view_GUI.addAdunareListener(new AdunareListener());
		view_GUI.addScadereListener(new ScadereListener());
		view_GUI.addInmultireListener(new InmultireListener());
		view_GUI.addImpartireListener(new ImpartireListener());
		view_GUI.addIntegrareP1Listener(new IntegrareListener1());
		view_GUI.addIntegrareP2Listener(new IntegrareListener2());
		view_GUI.addDerivareP1Listener(new DerivareListener1());
		view_GUI.addDerivareP2Listener(new DerivareListener2());
		
	}
	//Clasa pentru actiunea butonului de adunare
	class AdunareListener implements ActionListener
	{

		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 && m_view.getPolinom2Text().length() > 0)
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele sunt introduse corect
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.adunare(p1, p2);//Rezultatul adunarii celor doua polinoame
					String rez_s = m_model.convert(rez);//Convertirea la String
					rez_s = rez_s.replace(".0", "");//Stergerea substringului ".0" deoarece in cadrul adunarii este vorba de coeficienti intregi
					m_view.setRezultat(rez_s);
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de scadere
	class ScadereListener implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 && m_view.getPolinom2Text().length() > 0)
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele introduse sunt corecte
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.scadere(p1, p2);//Rezultatul scaderii celor doua polinoame
					String rez_s = m_model.convert(rez);//Convertirea la String
					rez_s = rez_s.replace(".0", "");//Stegerea substringului ".0" deoarece in cadrul scaderii este vorba de coeficienti intregi
					m_view.setRezultat(rez_s);
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de inmultire
	class InmultireListener implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 && m_view.getPolinom2Text().length() > 0)
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele sunt introduse corect
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.inmultire(p1, p2);//Rezultatul inmultirii celor doua polinoame
					String rez_s = m_model.convert(rez);//Convertirea la String
					rez_s = rez_s.replace(".0", "");//Stergerea substringului ".0" deoarece in cadrul inmultirii este vorba de coeficienti intregi
					m_view.setRezultat(rez_s);
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de impartire
	class ImpartireListener implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				Polinom rest = new Polinom();
				if(m_view.getPolinom1Text().length() > 0 && m_view.getPolinom2Text().length() > 0)
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele de intrare sunt introduse corect	
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.impartire(p1, p2, rest);//Catul impartirii celor doua polinoame
					m_view.setRezultat(m_model.convert(rez));//Convertirea la String + Setarea
					m_view.setRest(m_model.convert(rest));
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de derivare1
	class DerivareListener1 implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 )
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele de intrare sunt introduse corect
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					rez = m_model.derivare(p1);//Rezultatul derivarii polinomului
					String rez_s = m_model.convert(rez);//Convertirea la String
					rez_s = rez_s.replace(".0", "");//Stergerea substringului ".0" deoarece in cazul derivarii este vorba de coeficienti intregi
					m_view.setRezultat(rez_s);
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de derivare2
	class DerivareListener2 implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom2Text().length() > 0 )
				{
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.derivare(p2);
					m_view.setRezultat(m_model.convert(rez));
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de integrare1
	class IntegrareListener1 implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 )
				{
					m_model.verificareString(m_view.getPolinom1Text());//Verificam daca datele de intrare au fost introduse corect
					Polinom p1 = new Polinom(m_view.getPolinom1Text());
					rez = m_model.integrare(p1);//Rezultatul integrarii polinomului
					if(rez.lista_monoame.get(0).get_coeficient() == 0)
						m_view.setRezultat(m_model.convert(rez));
					else
					m_view.setRezultat(m_model.convert(rez) + "+c");//Convertirea la String + Setarea
					
					m_view.clearRest();
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
	//Clasa pentru actiunea butonului de integrare2
	class IntegrareListener2 implements ActionListener
	{
		//Metoda care se va apela in momentul apasarii butonului
		public void actionPerformed(ActionEvent e)
		{
			try 
			{
				Polinom rez;
				if(m_view.getPolinom1Text().length() > 0 )
				{
					m_model.verificareString(m_view.getPolinom2Text());
					Polinom p2 = new Polinom(m_view.getPolinom2Text());
					rez = m_model.integrare(p2);
					if(rez.lista_monoame.get(0).get_coeficient() == 0)
						m_view.setRezultat(m_model.convert(rez));
					else
						m_view.setRezultat(m_model.convert(rez) + "+c");
				}
				else
					throw new Exception();
			}
			catch(Exception e2)
			{
				//In cazul in care datele au fost introduse gresit
				m_view.showError("Datele sunt introduse gresiti! Verificati!");
			}
			
		}
	}
}