package polinom_pro;

import javax.swing.JOptionPane;

public class Operatii {
	//Metoda de adunarea a doua polinoame returnand polinomul rezultat
	Polinom adunare (Polinom p1, Polinom p2)
	{
		Polinom p3 = new Polinom();
		p3 = p3.polinom_cpy(p1,p2, 1);//Concatenam p1 si p2
		p3.sortare_lista();//Sortam noul polinom
		p3.rectificare(p3);//Il rectificam => adunam
		return p3;
	}
	//Metoda de scadere a doua polinoame returnand polinomul rezultat
	Polinom scadere (Polinom p1, Polinom p2)
	{
		Polinom p3 = new Polinom();
		p3 = p3.polinom_cpy(p1, p2, 0);//Concatenam p1 si p2, p2 avand monoamele negate
		p3.sortare_lista();//Sortam noul polinom
		p3.rectificare(p3);//Il rectificam => scadere
		if(p3.lista_monoame.isEmpty())//In cazul in care avem rezultatul 0 construim un monom (0,0) pe care il adaugam rezultatului pentru o mai buna afisare
			p3.lista_monoame.add(new Monom(0, 0));
		return p3;
		
	}
	//Metoda de inmultire a doua polinoame returnand polinomul rezultat
	Polinom inmultire (Polinom p1, Polinom p2)
	{
		Polinom p3 = new Polinom();
		for (Monom x : p1.lista_monoame)//Produsul fiecarui monom din p1 cu fiecare monom din p2
			for (Monom y : p2.lista_monoame)
			{
				
				p3.lista_monoame.add(new Monom(x.get_coeficient() * y.get_coeficient(), x.get_grad() + y.get_grad()));
			}
		p3.sortare_lista();//Sortam rezultatul
		p3.rectificare(p3);//Il rectificam
		if(p3.lista_monoame.isEmpty())//In cazul in care avem rezultatul 0 construim un monom (0,0) pe care il adaugam rezultatului penru o mai buna afisare
			p3.lista_monoame.add(new Monom (0, 0));
		return p3;
	}
	//Metoda de impartire a doua polinoame, returnand polinomul rezultat ca si cat si modificand polinomul rest
	Polinom impartire(Polinom p1, Polinom p2, Polinom rest)
	{
		Polinom p_rez = new Polinom();
		Operatii o = new Operatii();
		Polinom rez_i = new Polinom();
		Polinom aux = new Polinom();
		
		if(p1.lista_monoame.get(0).get_grad() < p2.lista_monoame.get(0).get_grad())
		{
			//In cazul in care impartim un polinom mai mic la unul mai mare
			for(Monom x : p1.lista_monoame)
			{
				//Rotunjirea rezultatului la doua zecimale pentru o mai buna afisare
				double coef = x.get_coeficient();
				coef = o.reducere_numar_zecimale(coef);
				rest.lista_monoame.add(new Monom (coef, x.get_grad()));
			}
			p_rez.lista_monoame.add(new Monom(0, 0));
			
			return p_rez;
		}
		//Aux folosit ca si copie a lui p1 care urmeaza sa fie modificata
		for (Monom x : p1.lista_monoame)
			aux.lista_monoame.add(new Monom(x.get_coeficient(), x.get_grad()));
		
		while (aux.lista_monoame.get(0).get_grad() >= p2.lista_monoame.get(0).get_grad())
		{	
			//Impartirea primului monom din aux cu primul monom din p2
			Monom caux = new Monom (aux.lista_monoame.get(0).get_coeficient()/p2.lista_monoame.get(0).get_coeficient(), aux.lista_monoame.get(0).get_grad() - p2.lista_monoame.get(0).get_grad());
			p_rez.lista_monoame.add(new Monom (caux.get_coeficient(), caux.get_grad()));
			Polinom pol_caux = new Polinom();
			pol_caux.lista_monoame.add(new Monom (caux.get_coeficient(), caux.get_grad()));
			rez_i = o.inmultire(pol_caux, p2);//Catul auxiliar, caux, este inmultit cu p2, impartitorul
			pol_caux.lista_monoame.remove(0);//Stergem monomul din lista de monoame ale polinomului pol_aux deoarece acesta va trebui sa contina doar un monom la fiecare pas
			aux = o.scadere(aux, rez_i);//Scadem din aux, rezultatul inmultirii
			if(aux.lista_monoame.get(0).get_coeficient() == 0)
			
				break;
		}
		for (Monom x : aux.lista_monoame)
			rest.lista_monoame.add(new Monom(x.get_coeficient(), x.get_grad()));
		
		//Rotunjirea la doar doua zecimale ale coeficientilor pentru o mai buna afisare
		for (Monom x : p_rez.lista_monoame)
		{
			double coef = x.get_coeficient();
			coef = o.reducere_numar_zecimale(coef);
			x.set_coeficient(coef);
		}
		return p_rez;
	}
	//Metoda de derivare a polinomului, returnand polinomul rezultat
	Polinom derivare (Polinom p)
	{
		Polinom p_rez = new Polinom();
		if (p.lista_monoame.isEmpty())
		{
			p_rez.lista_monoame.add(new Monom (0,0));
			return p_rez;
		}
		
		for (Monom x : p.lista_monoame)
		{
			//Testam daca avem de a face cu o constanta
			if(x.get_grad() == 0)
				if(p.lista_monoame.size() >1);
				else
					p_rez.lista_monoame.add(new Monom(0,0));//Constanta=> o sa avem rezultatul la derivare 0
			else
				p_rez.lista_monoame.add(new Monom(x.get_coeficient()*x.get_grad(), x.get_grad() - 1));//Un monom care are gradul >= 1
					
						
		}
		p_rez.sortare_lista();//Sortam polinomul rezultat
		p_rez.rectificare(p_rez);//Rectificam polinomul rezultat
		return p_rez;
	}
	//Metoda de integrare a polinomului, returnand polinomul rezultat
	Polinom integrare (Polinom p)
	{
		Polinom p_rez = new Polinom();
		
		if (p.lista_monoame.isEmpty())
		{
			p_rez.lista_monoame.add(new Monom (0,0));
			return p_rez;
		}
		
		for (Monom x : p.lista_monoame)
			if(x.get_grad() > 0)
			{
				p_rez.lista_monoame.add(new Monom(x.get_coeficient()/(x.get_grad()+1), x.get_grad() + 1));
			}
			else
				p_rez.lista_monoame.add(new Monom(x.get_coeficient(), x.get_grad() + 1));////In cazul in care gradul=0
				
		p_rez.sortare_lista();//Sortam polinomul rezultat
		p_rez.rectificare(p_rez);//Rectificam polinomul rezultat
		Operatii o = new Operatii();
		//Rotunjirea rezultatului la doua zecimale pentru o mai buna afisare
		for (Monom x : p_rez.lista_monoame)
		{
			double coef = x.get_coeficient();
			coef = o.reducere_numar_zecimale(coef);
			x.set_coeficient(coef);
		}
		
		return p_rez;
	}
	//Metoda de conversie a unui polinom la un String, returnand Stringul
	String convert(Polinom p1)
	{
		String s = " ";//Se ia un string cu un spatiu deoarece va trebui sa ne folosim de primul element => Nu poate fi NULL
		
		for (Monom x : p1.lista_monoame)
		{
			
			
			if(x.get_coeficient() == 1 )
				if(x.get_grad() == 1)
						s = s.substring(0,s.length()) + "+x";
				else
						if(x.get_grad() == 0)
							s = s.substring(0, s.length()) + "+1";
						else
							s = s.substring(0,s.length()) + "+x" + "^" + x.get_grad();
			else
				if(x.get_coeficient() == -1)
					if(x.get_grad() == 1)
							s = s.substring(0, s.length()) + "-x";
					else
						if(x.get_grad() == 0)
								s = s.substring(0, s.length()) + "-1";
						else
							s = s.substring(0, s.length())  + "-x" + "^" + x.get_grad();
				else
					if(x.get_grad() == 1)
						if(x.get_coeficient() > 0)
							s = s.substring(0, s.length()) + "+" + x.get_coeficient() + "x";
						else
							s = s.substring(0, s.length()) + x.get_coeficient() + "x";
					else
						if(x.get_grad() == 0)
							if(x.get_coeficient() > 0)
								s = s.substring(0, s.length()) + "+" + x.get_coeficient();
							else
								s = s.substring(0, s.length()) + x.get_coeficient();
						else
							if(x.get_coeficient() > 0)
								s = s.substring(0, s.length()) + "+" + x.get_coeficient() + "x" + "^" + x.get_grad();
							else
								s = s.substring(0, s.length()) + x.get_coeficient() + "x" + "^" + x.get_grad();
		}
		s = s.substring(1,s.length());
		if(s.charAt(0) == '+')
			s = s.substring(1, s.length());
		return s;
		
	}
	//Metoda prin care se verifica daca Stringul este introdus corect, in caz contrat vom arunca Exceptie
	void verificareString(String s) throws Exception
	{

		
		int i = 0;
		int ultim = s.length() - 1;
		
		if(s.charAt(ultim) != 'x' && (s.charAt(ultim) <'1' && s.charAt(ultim) > '9'))
			throw new Exception("Gresit");
		else
			if(s.charAt(0) != 'x' && (s.charAt(0) <'1' && s.charAt(0) >'9'))
				throw new Exception("Gresit");
		
		for (i = 1; i < s.length(); i++)
		{
						
			if(s.charAt(i)== 'x')
				if(s.charAt(ultim) != 'x')
					if((s.charAt(i + 1) != '^') && (s.charAt(i + 1) != '+') && (s.charAt(i + 1) != '-'))
					{
						throw new Exception("Gresit");
					}
					else;
				else;
					
			else
				if(s.charAt(i) == '^')
					if(s.charAt(i) != s.charAt(ultim))
					if(s.charAt(i + 1) < '1' && s.charAt(i + 1) > '9')
					{
						throw new Exception("Gresit");
					}
					else;
				else
					if(s.charAt(i) == '+' || s.charAt(i) == '-')
						if(s.charAt(i) != s.charAt(ultim))
						if(s.charAt(i + 1) == 'x' || (s.charAt(i + 1) < '1' && s.charAt(i + 1) > '9'))
						{
							throw new Exception("Gresit");
						}
						else;
			
					else
						if(s.charAt(i) != s.charAt(ultim))
							if(s.charAt(i + 1) == '^')
							{
								throw new Exception ("Gresit");
							}
								
				}	
		}
	//Metoda de reducere a numarului de zecimale pentru o mai buna afisare care returneaza numarul rotunjit
	double reducere_numar_zecimale (double a)
	{
		double rezultat = a;
		rezultat = Math.round(rezultat * 100);
		rezultat = rezultat / 100;
		return rezultat;
	}
}

