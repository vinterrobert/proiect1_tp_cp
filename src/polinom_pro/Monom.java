package polinom_pro;
//Avem clasa Monom.Un Monom are un coeficient si un grad
public class Monom {
	 private double coeficient;
	 private int grad;
	 //Constructor cu parametrii
	 public Monom (double coeficient, int grad)
	 {
		this.coeficient = coeficient;
		this.grad = grad;
	 }
	 //Getter al coeficientului
	 double get_coeficient ()
	 {
		 return this.coeficient;
	 }
	 //Getter al gradului
	 int get_grad()
	 {
		 return this.grad;
	 }
	 //Setter al coeficientului
	 void set_coeficient (double coeficient)
	 {
		 this.coeficient = coeficient;
	 }
	 //Setter al gradului
	 void set_grad (int grad)
	 {
		 this.grad  = grad;
	 }
	 //Functie de comparare a monoamelor in functie de grad
	 int compareTo(Monom mon)
	 {
		 if(this.get_grad() > mon.get_grad())
		 {
			 return 1;
		 }
		 else
		 {
			 if(this.get_grad() == mon.get_grad())
				 return 0;
			 else
				 return -1;
		 }
	 }
	 //Crearea unei clone a Monomului respectiv
	 public Monom clone()
	 {
		 return new Monom(coeficient, grad);
	 }
		 
}
