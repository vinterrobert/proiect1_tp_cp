package polinom_pro;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class View extends JFrame {
	//Declararea de butoane, textfield-uri
	//static final String INITIAL_VALUE = "1";
	private JTextField polinom1 = new JTextField(20);
	private JTextField polinom2 = new JTextField(20);
	private JTextField rezultat = new JTextField(20);
	private JTextField rest = new JTextField(20);
	private JButton adunare = new JButton ("ADUNARE");
	private JButton scadere = new JButton ("SCADERE");
	private JButton inmultire = new JButton ("INMULTIRE");
	private JButton impartire = new JButton ("IMPARTIRE");
	private JButton integrareP1 = new JButton ("INTEGRARE P1");
	private JButton integrareP2 = new JButton ("INTEGRARE P2");
	private JButton derivareP1 = new JButton ("DERIVARE P1");
	private JButton derivareP2 = new JButton ("DERIVARE P2");
	
	public View()
	{
		//Declararea Panel-urilor
		JPanel content = new JPanel();
		JPanel contentP1P2 = new JPanel();
		JPanel contentRezRest = new JPanel();
		JPanel contentAdunare = new JPanel();
		JPanel contentScadere = new JPanel();
		JPanel contentInmultire = new JPanel();
		JPanel contentImpartire = new JPanel();
		JPanel contentIntegrareP1 = new JPanel();
		JPanel contentIntegrareP2 = new JPanel();
		JPanel contentDerivareP1 = new JPanel();
		JPanel contentDerivareP2 = new JPanel();
		
		//Setarea Panel-urilor 
		content.setLayout(new GridLayout(0,1));
		contentP1P2.setLayout(new GridLayout(0,2));
		contentRezRest.setLayout(new GridLayout(0,2));
		contentAdunare.setLayout(new FlowLayout());
		contentScadere.setLayout(new FlowLayout());
		contentInmultire.setLayout(new FlowLayout());
		contentImpartire.setLayout(new FlowLayout());
		contentIntegrareP1.setLayout(new FlowLayout());
		contentIntegrareP1.setLayout(new FlowLayout());
		contentDerivareP1.setLayout(new FlowLayout());
		contentDerivareP2.setLayout(new FlowLayout());

		//Adaugarea butoanelor si a Label-urilor la Panel-uri
		contentP1P2.add(new JLabel("Polinom 1"));
		contentP1P2.add(polinom1);
		
		contentP1P2.add(new JLabel("Polinom 2"));
		contentP1P2.add(polinom2);
		
		
		contentRezRest.add(new JLabel("Rezultat"));
		contentRezRest.add(rezultat);
		contentRezRest.add(new JLabel("Rest"));
		contentRezRest.add(rest);
		
		contentAdunare.add(adunare);
		contentScadere.add(scadere);
		contentInmultire.add(inmultire);
		contentImpartire.add(impartire);
		contentIntegrareP1.add(integrareP1);
		contentIntegrareP2.add(integrareP2);
		contentDerivareP1.add(derivareP1);
		contentDerivareP2.add(derivareP2);
		
		content.add(contentP1P2);
		content.add(contentRezRest);
		content.add(contentAdunare);
		content.add(contentScadere);
		content.add(contentInmultire);
		content.add(contentImpartire);
		content.add(contentIntegrareP1);
		content.add(contentIntegrareP2);
		content.add(contentDerivareP1);
		content.add(contentDerivareP2);
		
		
		
		this.setContentPane(content);
		this.pack();
		this.setTitle("Operatii Polinoame");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	//Metoda de preluare a textului din textbox1
	String getPolinom1Text()
	{
		return polinom1.getText();
	}
	//Metoda de preluare a textului din textbox2
	String getPolinom2Text()
	{
		return polinom2.getText();
	}
	//Metoda de setare a textbox-ului corespunzator rezultatului
	void setRezultat(String s)
	{
		rezultat.setText(s);
	}
	//Metoda de setare a textbox-ului corespunzator restului
	void setRest(String s)
	{
		rest.setText(s);
	}
	//Metoda de curatare a textbox-ului corespunzator restului
	void clearRest ()
	{
		rest.setText("");
	}
	//Metoda prin care apare mesajul de eroare
	void showError(String s)
	{
		JOptionPane.showMessageDialog(this, s);
	}
	//Metoda de setare a textbox-ului corespunzator rezultatului
	void addAdunareListener (ActionListener add)
	{
		adunare.addActionListener(add);
	}
	void addScadereListener (ActionListener sub)
	{
		scadere.addActionListener(sub);
	}
	void addInmultireListener (ActionListener mul)
	{
		inmultire.addActionListener(mul);
	}
	void addImpartireListener (ActionListener imp)
	{
		impartire.addActionListener(imp);
	}
	void addIntegrareP1Listener (ActionListener int1)
	{
		integrareP1.addActionListener(int1);
	}
	void addIntegrareP2Listener (ActionListener int2)
	{
		integrareP2.addActionListener(int2);
	}
	void addDerivareP1Listener (ActionListener der1)
	{
		derivareP1.addActionListener(der1);
	}
	void addDerivareP2Listener (ActionListener der2)
	{
		derivareP2.addActionListener(der2);
	}
}
