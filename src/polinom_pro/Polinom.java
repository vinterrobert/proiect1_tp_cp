package polinom_pro;
import java.util.*;
//Clasa Polinom. Un polinom este format din mai multe monoame
public class Polinom {
	
	ArrayList <Monom> lista_monoame;
	//Constructor cu parametru
	public Polinom(String s)
	{
		
		this.lista_monoame = new ArrayList <Monom>();
		populare_ArrayList(this.lista_monoame ,s);
		sortare_lista ();
		rectificare (this);
	
		
	}
	//Constructor fara parametru
	public Polinom ()
	{
		this.lista_monoame = new ArrayList <Monom>();
	}
	
	//Metoda de construire a ArrayListului
	void populare_ArrayList(ArrayList<Monom> l, String s)
	{
		
		String[] s2;
		int i;
		double coeficient = 0;
		int grad = 0;
		
		//Inseram + in fata fiecarui minus
		
		
		for (i = 1; i < s.length(); i++)
		{
			if (s.charAt(i) == '-')
			{
				s = s.substring(0, i) + '+' + s.substring(i, s.length());
				i++;
			}
		}
		//Despartim stringul principal in substringuri
		
		s2 = s.split("[+]");
	
		//Construim monoamele si populam lista
		
		for (i = 0; i < s2.length; i++)
		{
			int j = 0;
			if(s2[i].length() > 0)
				if(s2[i].charAt(0) == '-')
				{
					if(s2[i].charAt(1) != 'x')
					{
						j=1;
						while(s2[i].charAt(j) != 'x' && j < s2[i].length())
						{
							if (j + 1 < s2[i].length())
								j++;
							else
							{
								j++;
								break;
							}
						}
					
						coeficient =(-1) * Double.parseDouble(s2[i].substring(1, j));
					
						if(j + 2 < s2[i].length())
							grad = Integer.parseInt(s2[i].substring(j + 2, s2[i].length()));
						else
							if(j == s2[i].length())
								grad = 0;
							else grad = 1;
							
					}	
					else
						{coeficient = -1;
						 j = 1;
						 if(j + 2 < s2[i].length())
							 grad = Integer.parseInt(s2[i].substring(j + 2, s2[i].length()));
						 else
							 grad = 1;
						}
				}
			
			else
				if(s2[i].charAt(0) == 'x')
				{
					coeficient = 1;
					if(s2[i].length() > 1)
							grad = Integer.parseInt(s2[i].substring(2, s2[i].length()));
						else
							grad = 1;
						
				}
				else
				{
					j = 0;
					
					while(s2[i].charAt(j)!= 'x' && j < s2[i].length())
					{
						if(j + 1 < s2[i].length())
							j++;
						else
						{
							j++;
							break;
						}
					}
					coeficient = Double.parseDouble(s2[i].substring(0, j));
					
					
					if (j != s2[i].length())
					if(s2[i].charAt(j) == 'x')
					if(j + 2 < s2[i].length())
							grad = Integer.parseInt(s2[i].substring(j + 2, s2[i].length()));
						else
							grad = 1;
					else 
						grad = 0;
					else 
						grad = 0;
					
				}
			if(coeficient != 0)
				this.lista_monoame.add(new Monom(coeficient, grad));	
		}
		
	}
	
	//Metoda prin care sortam lista de monoame
	void sortare_lista()
	{
		Collections.sort(this.lista_monoame, new Comparator<Monom>()
		{
			public int compare(Monom mon1, Monom mon2)
			{
				return mon2.compareTo(mon1);
			}
		});
	}
	//Metoda prin care toate monoamele de acelasi grad se aduna
	void rectificare(Polinom p)
	{
		boolean ok;
		int index = 0;
		do
		{
			ok = true;
			for (index = 0; index < p.lista_monoame.size() - 1; index++)
			{
				Monom x = p.lista_monoame.get(index);
				Monom y = p.lista_monoame.get(index + 1);
				if(x.get_grad() == y.get_grad())
				{
					x.set_coeficient(x.get_coeficient() + y.get_coeficient());
					p.lista_monoame.remove(index + 1);
					ok = false;
					if(x.get_coeficient() == 0)
						p.lista_monoame.remove(index);
					index++;
				}
				
			}
		}while(!ok);			
	}
	
	
	//Concatenarea celor 2 polinoame intr-un polinom nou. Rolul variabilei semn este de a decide semnul monoamelor din polinomul p2
	Polinom polinom_cpy (Polinom p1, Polinom p2, int semn)
	{
		Polinom p_rez1 = new Polinom();
		Polinom p_rez2 = new Polinom();
		for (Monom x : p1.lista_monoame)
		{
			p_rez1.lista_monoame.add(new Monom (x.get_coeficient(), x.get_grad()));
		}
		
		for(Monom x : p2.lista_monoame)
		{
			p_rez2.lista_monoame.add(new Monom (x.get_coeficient(), x.get_grad()));
		}
		
		if(semn == 1)
			for (Monom x : p_rez2.lista_monoame)
			{
				p_rez1.lista_monoame.add(new Monom(x.get_coeficient(), x.get_grad()));
			}
		else
			for (Monom x : p_rez2.lista_monoame)
			{
				p_rez1.lista_monoame.add(new Monom(-(x.get_coeficient()), x.get_grad()));
			}
		
		return p_rez1;
	}
	
}

